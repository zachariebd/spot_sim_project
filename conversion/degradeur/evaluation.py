#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import json
import shutil
import argparse
import tempfile
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
from matplotlib.ticker import PercentFormatter
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick


def evaluation(eval_path,list_converted_bands_files,target_sat,target_ins,out_path,dictionary,method):
    
            
    if method == "snow" :  snowmap(eval_path,list_converted_bands_files,target_sat,target_ins,out_path,dictionary)
        
        
    
def snowmap(eval_path,list_converted_bands_files,target_sat,target_ins,out_path,dictionary):
    
    
    # Plot figure with subplots 
    fig, (ax1,ax2,ax3) = plt.subplots(3, sharex=True, sharey=False)
    
    fig.suptitle("HISTOGRAM",size = 14)
    # set up subplot grid
    ax1.set_title('all pixels')
    ax2.set_title('snow pixels')
    ax3.set_title('cloud pixels')
    ax3.set(xlabel='reflectance',ylabel='Percent')
    ax1.set(ylabel='Percent')
    ax2.set(ylabel='Percent')
    
    
    
    
    
    for i in list_converted_bands_files:
        band = i[0]
        band_path = i[1]
        target_res = float(dictionary[target_sat][target_ins]["BANDS"][band]["RESOLUTION"])
     
        #snow_raster = gdal.Translate('',eval_path,format= 'MEM',noData = 255,outputType = gdal.GDT_Float32)
        #snow_raster = gdal.Warp('',snow_raster,format= 'MEM', dstNodata = 9999)
        #snow_raster = gdal.Translate('',snow_raster,format= 'MEM',noData = 205)
        #snow_raster = gdal.Warp('',snow_raster,format= 'MEM', dstNodata = 9999)
        #snow_raster = gdal.Translate('',snow_raster,format= 'MEM',noData = 100000)

        snow_raster = gdal.Open(eval_path)
   
        snow_array = BandReadAsArray(snow_raster.GetRasterBand(1)).flatten()
        
        band_raster = gdal.Open(band_path)
        band_array = BandReadAsArray(band_raster.GetRasterBand(1)).flatten()
        cond = np.where((band_array >0) & (band_array< 12500))
        snow_array = snow_array[cond]
        band_array = band_array[cond]
        
        #cond = np.where((snow_array <= 100 ) & (snow_array> 0))
        band_array_snow = band_array[snow_array == 100]
        band_array_cloud = band_array[snow_array == 205]
        ax1.hist(band_array,bins=50,alpha=0.3,label=band,weights=np.ones(len(band_array)) / len(band_array))
        ax2.hist(band_array_snow,bins=50,alpha=0.3,label=band,weights=np.ones(len(band_array_snow)) / len(band_array_snow))
        ax3.hist(band_array_cloud,bins=50,alpha=0.3,label=band,weights=np.ones(len(band_array_cloud)) / len(band_array_cloud))
    ax1.yaxis.set_major_formatter(mtick.PercentFormatter(1))
    ax2.yaxis.set_major_formatter(mtick.PercentFormatter(1))
    ax3.yaxis.set_major_formatter(mtick.PercentFormatter(1))
    #plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
    ax1.legend()
    ax2.legend()
    ax3.legend()
    snow_histo_path = op.join(out_path,target_sat+"_"+target_ins+"_SNOW.png")
    fig.savefig(os.path.join(out_path,snow_histo_path))
    plt.close(fig)
        
        
        
            
        
        
        
    
