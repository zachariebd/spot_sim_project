#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import errno
import re
import json
import shutil
import glob
import argparse
import tempfile
import format_conversion
import evaluation
import band_conversion
from datetime import datetime, timedelta, date

def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise


def getDateFromStr(N):
    sepList = ["","-","_"]
    YYYYMMDD = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           YYYYMMDD = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return YYYYMMDD

    

def main():
    
    script_path = os.path.dirname(os.path.abspath(__file__))
    dictionary_path = op.join(script_path,"..","librairie_instruments","satellite_library.json")
    temp_path = op.join(script_path,"..","temp")
    mkdir_p(temp_path)
    out_path = op.join(script_path,"..","output")
   
    
    # parsing arguments#######################################################################
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', action='store',dest='input_image_path',default="", help='input satellite image; as one-band rasters')
    parser.add_argument('-on', action='store',dest='output_name',default=".", help='output sub directory name')
    parser.add_argument('-ts', action='store', default="", dest='target_sat',help='name of the target satellite')
    parser.add_argument('-ti', action='store', default="",dest='target_ins', help='name of the target instrument')
    parser.add_argument('-is', action='store', dest='input_sat',help='name of the input satellite')
    parser.add_argument('-ii', action='store', dest='input_ins',help = 'name of the input instrument')
    parser.add_argument('-bc', action='store', default="",dest='band_conversion_method', help='mode of band conversion')
    parser.add_argument('-fc', action='store', default="",dest='format_conversion_method', help='mode of format conversion')
    parser.add_argument('-sm', action='store', default="",dest='snow_map_path', help='lis snow map')
    parser.add_argument('-eval', action='store', default="", dest='eval_image',help='satellite image used for evaluating the output; either as a multi-band raster or as a dir with one-band rasters')


    arguments = parser.parse_args()
    input_image_path = arguments.input_image_path
    output_name = arguments.output_name
    input_sat = arguments.input_sat
    input_ins = arguments.input_ins
    target_sat = arguments.target_sat
    target_ins = arguments.target_ins
    band_conversion_method = arguments.band_conversion_method
    format_conversion_method = arguments.format_conversion_method
    snow_map_path = arguments.snow_map_path
    eval_image = arguments.eval_image

    out_path = op.join(out_path,output_name)
    #os.system('rm -f'+out_path+"/*")
    mkdir_p(out_path)
    
    
    
    dictionary_file =  open(dictionary_path)
    dictionary = json.load(dictionary_file)
    do_eval = None
    do_snow = None

    
    #check validity of arguments###################################

    
    if not op.isdir(input_image_path):
        print("Please enter an existing input image dir path")
        return
    
    if eval_image == "":
        do_eval = False
    elif not op.isdir(eval_image):
        print("Please enter an existing eval image dir path")
        return
    else: 
        do_eval = True

    if snow_map_path =="":
        do_snow = False
    elif not op.isfile(snow_map_path):
        print("Please enter an existing snow map path")
        return
    else: 
        do_snow = True

    
    if input_sat not in dictionary:
        print(input_sat + " not in the dictionary")
        return
    elif input_ins not in dictionary[input_sat] :
        print(input_ins + " not in "+ input_sat)
        return
    elif "SENTINEL" not in input_sat :
        print("ne marche que avec SENTINEL 2 en input")
        return
        
    if target_sat not in dictionary:
        print(target_sat + " not in the dictionary")
        return
    elif target_ins not in dictionary[target_sat] :
        print(target_ins + " not in "+ target_sat)
        return
    elif "SPOT" not in target_sat :
        print("ne marche que avec SPOT en target")
        return
        
        
    #get date of input image
    date_input = getDateFromStr(input_image_path)

    # search output bands
    list_target_bands = []
    for b in dictionary[target_sat][target_ins]["BANDS"]:
        list_target_bands.append(b)

    # search overlapping input bands and rasters
    list_input_bands = []
    list_input_bands_paths = [] 
    for target_band in list_target_bands:
        target_center = float(dictionary[target_sat][target_ins]["BANDS"][target_band]["BAND CENTER"])
        target_width = float(dictionary[target_sat][target_ins]["BANDS"][target_band]["BAND WIDTH"])
        target_min = target_center - target_width/2
        target_max = target_center + target_width/2
        for input_band in dictionary[input_sat][input_ins]["BANDS"]:
            input_center = float(dictionary[input_sat][input_ins]["BANDS"][input_band]["BAND CENTER"])
            input_width = float(dictionary[input_sat][input_ins]["BANDS"][input_band]["BAND WIDTH"])
            input_min = input_center - input_width/2
            input_max = input_center + input_width/2
            overlap_min = max(input_min,target_min)
            overlap_max = min(input_max,target_max)
            if overlap_min < overlap_max:
                input_suffix = dictionary[input_sat][input_ins]["BANDS"][input_band]["SUFFIX"]
                list_input_bands.append(input_band)
                list_input_bands_paths.append(glob.glob(op.join(input_image_path,'**','*'+input_suffix+'*'), recursive=True)[0])
        


    
    #band conversion
    list_converted_band_paths = band_conversion.average(input_image_path,input_sat,input_ins,target_sat,target_ins,list_input_bands,list_target_bands,out_path,dictionary)
    
    
    #format conversion
    list_new_bands_path = format_conversion.direct(list_converted_band_paths,list_target_bands,input_sat,input_ins,target_sat,target_ins,out_path,dictionary,date_input)

    
    
    
    #if do_snow : evaluation(snow_map_path,list_converted_bands_files,target_sat,target_ins,out_path,dictionary,"snow")



if __name__ == '__main__':
    main()
