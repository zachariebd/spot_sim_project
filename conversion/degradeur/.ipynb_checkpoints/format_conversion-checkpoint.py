#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import json
import shutil
import argparse
import tempfile
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import glob
import numpy as np
from matplotlib.ticker import PercentFormatter
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches
from itertools import cycle
from matplotlib.ticker import MultipleLocator
import math


    
def direct(list_converted_band_paths,list_target_bands,input_sat,input_ins,target_sat,target_ins,out_path,dictionary,date_input) :
    print('format conversion')
    
    for converted_band_path, target_band in zip(list_converted_band_paths,list_target_bands) :
        print("band: ",target_band)
        month = date_input.month
        target_suffix = dictionary[target_sat][target_ins]["BANDS"][target_band]["SUFFIX"]
        target_max_ref_saturation = float(dictionary[target_sat][target_ins]["BANDS"][target_band]["INFO_TILES"]["31TCH"]["MAX_REF_SATURATION"][month-1])
        target_min_ref_saturation= float(dictionary[target_sat][target_ins]["BANDS"][target_band]["INFO_TILES"]["31TCH"]["MIN_REF_SATURATION"][month-1])
        formated_band_path = op.join(out_path,input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+"_"+target_band+"_FORMATED.tif")
        temp_DN_path = op.join(out_path,input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+"_"+target_band+"_DN.tif")
        print("original to DN")
        target_DN = gdal.Translate(temp_DN_path, converted_band_path, format='GTiff', outputType=gdal.GDT_Byte, scaleParams=[[target_min_ref_saturation*10000,target_max_ref_saturation*10000,0,255]])
        print("DN to target")
        formated_band = gdal.Translate(formated_band_path, target_DN, format='GTiff', outputType=gdal.GDT_UInt16,  scaleParams=[[0,255,target_min_ref_saturation*1000,target_max_ref_saturation*1000]])
        
        target_band_raster = gdal.Open(converted_band_path)
        
        
        # Plot figure 
        fig = plt.figure()
        plt.title("REFLECTANCE 16BITS "+target_ins+target_suffix,size = 14)
        plt.xlabel('reflectance x 10000')
        plt.ylabel('pixels count')
        array = BandReadAsArray(target_band_raster.GetRasterBand(1)).flatten()
        c = len(np.unique(array))
        plt.hist(array,bins=1000,label=str(c) +" unique values")
        histo_path = op.join(out_path,"HISTO_"+input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+target_suffix+".png")
        plt.grid()
        plt.legend()
        fig.set_size_inches(15.5, 8.5)
        fig.savefig(os.path.join(out_path,histo_path))
        plt.close(fig)
        
        fig = plt.figure()
        plt.title("REFLECTANCE 8BITS "+target_ins+target_suffix,size = 14)
        plt.xlabel('reflectance DN 8BITS')
        plt.ylabel('pixels count')
        array = BandReadAsArray(target_DN.GetRasterBand(1)).flatten()
        c = len(np.unique(array))
        plt.hist(array,bins=range(0,256),label=str(c) +" unique values")
        histo_path = op.join(out_path,"HISTO_8BITS_"+input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+target_suffix+".png")
        plt.grid()
        plt.legend()
        fig.set_size_inches(15.5, 8.5)
        fig.savefig(os.path.join(out_path,histo_path))
        plt.close(fig)
        
        fig = plt.figure()
        plt.title("REFLECTANCE 256 VALUES "+target_ins+target_suffix,size = 14)
        plt.xlabel('reflectance x 1000')
        plt.ylabel('pixels count')
        array = BandReadAsArray(formated_band.GetRasterBand(1)).flatten()
        c = len(np.unique(array))
        plt.hist(array,bins=1000, range=(0,target_max_ref_saturation*1000),label=str(c) +" unique values")
        histo_path = op.join(out_path,"HISTO_256_"+input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+target_suffix+".png")
        plt.grid()
        plt.legend()
        fig.set_size_inches(15.5, 8.5)
        fig.savefig(os.path.join(out_path,histo_path))
        plt.close(fig)
        
        
        
    
        

        
        
        
        
