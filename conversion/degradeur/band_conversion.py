#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import json
import shutil
import argparse
import tempfile
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import glob
import numpy as np
from matplotlib.ticker import PercentFormatter
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches
from itertools import cycle
from matplotlib.ticker import MultipleLocator


        
    
def nearest(input_image_path,input_sat,input_ins,target_sat,target_ins,list_input_bands,list_target_bands,out_path,library) :
    
    list_output_files = []
    print("nearest")
    for target_band in list_target_bands:
        target_suffix = library[target_sat][target_ins]["BANDS"][target_band]["SUFFIX"]
        target_center = float(library[target_sat][target_ins]["BANDS"][target_band]["BAND CENTER"])
        target_res = float(library[target_sat][target_ins]["BANDS"][target_band]["RESOLUTION"])
        min_diff = 10000
        nearest_input_band = ""
        print(target_sat,target_band)
        for input_band in list_input_bands:
            print(input_sat,input_band)
            input_center = float(library[input_sat][input_ins]["BANDS"][input_band]["BAND CENTER"])
            diff = abs(target_center - input_center)
            if diff < min_diff : 
                nearest_input_band = input_band
                min_diff = diff
        print("closest:",input_sat,nearest_input_band)
        input_suffix = library[input_sat][input_ins]["BANDS"][nearest_input_band]["SUFFIX"]
    
        input_band_path = glob.glob(op.join(input_image_path,'**','*'+input_suffix+'*'), recursive=True)[0]
        print(input_band_path)
        target_band_path = op.join(out_path,input_sat+"_"+input_ins+"_"+nearest_input_band+"_TO_"+target_sat+"_"+target_ins+"_"+target_band+".tif")
        list_output_files.append([target_band,target_band_path])
        temp_target = gdal.Translate('',input_band_path,format= 'MEM',noData = 0)
        gdal.Warp(target_band_path,temp_target,format= 'GTiff',resampleAlg="cubic",xRes= target_res,yRes= target_res)
    return list_output_files
        
        
        
def average(input_image_path,input_sat,input_ins,target_sat,target_ins,list_input_bands,list_target_bands,out_path,library) :
    
    list_output_files = []
    print("weighted average")
    fig = plt.figure()
    target_iterator = cycle([0.1,0,-0.1])
    
    for target_band in list_target_bands:
        target_suffix = library[target_sat][target_ins]["BANDS"][target_band]["SUFFIX"]
        target_center = float(library[target_sat][target_ins]["BANDS"][target_band]["BAND CENTER"])
        target_width = float(library[target_sat][target_ins]["BANDS"][target_band]["BAND WIDTH"])
        target_res = float(library[target_sat][target_ins]["BANDS"][target_band]["RESOLUTION"])
        target_min = target_center - target_width/2
        target_max = target_center + target_width/2
        total_overlap = 0
        total_band_array = None
        print("target",target_sat,target_band)
        target_name = library[target_sat][target_ins]["BANDS"][target_band]["NAME"].split()[0]
        if target_name == "pan" : continue
        if target_name == "blue" or target_name == "green" or target_name == "red" : color = target_name
        if "nir" in target_name : color = "firebrick"
        if "swir" in target_name : color = "darkorange"
        if "vegetation" in target_name : color = "peru"
        if "water" in target_name : color = "aqua"
        if "cirrus" in target_name : color = "grey"
        if "aerosol" in target_name : color = "magenta"
        i = next(target_iterator)
        plt.plot([target_min,target_max],[0+i,0+i], label = target_ins+target_suffix,color=color)
        if i >= 0:
            t = 0.01
        else: 
            t = -0.03
        plt.text(target_center,0 + i + t,target_band,fontsize='x-small')
        input_iterator = cycle([0.1,0,-0.1])
        for input_band in list_input_bands:
            
            input_center = float(library[input_sat][input_ins]["BANDS"][input_band]["BAND CENTER"])
            input_width = float(library[input_sat][input_ins]["BANDS"][input_band]["BAND WIDTH"])
            input_min = input_center - input_width/2
            input_max = input_center + input_width/2
            overlap_min = max(input_min,target_min)
            overlap_max = min(input_max,target_max)
        
            if overlap_min < overlap_max:
                print("input",input_sat,input_band)
                k = next(input_iterator)
                overlap = overlap_max - overlap_min
               
                total_overlap = total_overlap + overlap
                print("overlap",overlap,"total",total_overlap)
                input_suffix = library[input_sat][input_ins]["BANDS"][input_band]["SUFFIX"]
                input_band_path = glob.glob(op.join(input_image_path,'**','*'+input_suffix+'*'), recursive=True)[0]
                input_band_raster = gdal.Open(input_band_path)
                input_band_raster_res = gdal.Warp("",input_band_raster,format= 'MEM',resampleAlg="average",xRes= target_res,yRes= target_res)
                input_band_array = BandReadAsArray(input_band_raster_res.GetRasterBand(1))
                print(np.amax(input_band_array))
                weighted_band_array = overlap * input_band_array
                if  total_band_array is None:
                    total_band_array = weighted_band_array
                else:
                    total_band_array = total_band_array + weighted_band_array
                    
                input_name = library[input_sat][input_ins]["BANDS"][input_band]["NAME"].split()[0]
                if input_name == "pan" : continue
                if input_name == "blue" or input_name == "green" or input_name == "red" : color = input_name
                if "nir" in input_name : color = "firebrick"
                if "swir" in input_name : color = "darkorange"
                if "vegetation" in input_name : color = "peru"
                if "water" in input_name : color = "aqua"
                if "cirrus" in input_name : color = "grey"
                if "aerosol" in input_name : color = "magenta"
                plt.plot([input_min,input_max],[1+k,1+k], label = input_ins+input_suffix,color = color)
                plt.plot([overlap_min,overlap_min], [0+i,1+k], linestyle="--",color=color)
                plt.plot([overlap_max,overlap_max], [0+i,1+k], linestyle="--",color=color)
                if k >= 0:
                    t = 0.01
                else: 
                    t = -0.03
                plt.text(input_center,1 + k + t,input_band,fontsize='x-small')
        if total_overlap > 0:
            print("total overlap",total_overlap)
            average_band_array = total_band_array/total_overlap
            average_band_raster = input_band_raster_res
            average_band_raster.GetRasterBand(1).WriteArray(average_band_array)
            target_band_path = op.join(out_path,input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+"_"+target_band+".tif")
            list_output_files.append(target_band_path)
            gdal.Warp(target_band_path,average_band_raster,format= 'GTiff')
        
    plt.yticks([0,1], [target_ins,input_ins])
    plt.legend()
    bands_plot_path = op.join(out_path,input_sat+"_"+input_ins+"_TO_"+target_sat+"_"+target_ins+"_plot.png")
    fig.savefig(bands_plot_path)
    plt.close(fig)
                
    return list_output_files 
            
        
        
        
    
