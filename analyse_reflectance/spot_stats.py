#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import errno
from datetime import datetime, timedelta, date
import re
import json
import shutil
import argparse
import tempfile
import numpy as np
import glob
from matplotlib.ticker import PercentFormatter
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches
from itertools import cycle
from matplotlib.ticker import MultipleLocator
import zipfile
import xml.etree.ElementTree as ET

def getDateFromStr(N):
    sepList = ["","-","_"]
    YYYYMMDD = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           YYYYMMDD = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return YYYYMMDD

def getDateFromStr(N):
    sepList = ["","-","_"]
    YYYYMMDD = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           YYYYMMDD = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return YYYYMMDD

#grids for 31TCH
#tile = "31TCH"
#grid_max_K = 44
#grid_min_K = 40
#grid_max_J = 265
#grid_min_J = 262
tile = "31TGL"
grid_max_K = 52
grid_min_K = 49
grid_max_J = 259
grid_min_J = 256

spot_names = ["SPOT1","SPOT2","SPOT3"]
ins_names = ["HRV1","HRV2"]


SWH_path = "/datalake/theia/prod/SPOTWORLDHERITAGE"


#loop throught SPOTWORLDHERITAGE data
dict_ref = {}
for spot_name in spot_names:
    dict_ref[spot_name] = {}
    for ins_name in ins_names:
        dict_ref[spot_name][ins_name] = {}
        dict_ref[spot_name][ins_name]['years'] = {}
        dict_ref[spot_name][ins_name]['months'] = {}
found_scenes   = []     
for K in range(grid_min_K,grid_max_K+1):
    for J in range(grid_min_J,grid_max_J+1):
        print("looking of spot scenes in ",K, J,"grid")
        found_scenes = found_scenes + glob.glob(op.join(SWH_path,'**','*'+"_0"+str(K)+"-"+str(J)+'*.zip'), recursive=True)
for scene_path in found_scenes:
    zip_scene = zipfile.ZipFile(scene_path)
    for ziped_file in zip_scene.namelist():
        if ".xml" in ziped_file:
            date_scene = getDateFromStr(ziped_file)
            year = int(date_scene.year)
            month = int(date_scene.month)
            f = zip_scene.read(ziped_file)
            tree= ET.fromstring(f)
            ref_max_XS1 = 0
            ref_min_XS1 = 0
            ref_max_XS2 = 0
            ref_min_XS2 = 0
            ref_max_XS3 = 0
            ref_min_XS3 = 0
            for t in tree.findall(".//Band_Index_List[@band_id='XS1']/QUALITY_INDEX[@name='ReflectanceMax']"):
                ref_max_XS1 = float(t.text)
            for t in tree.findall(".//Band_Index_List[@band_id='XS1']/QUALITY_INDEX[@name='ReflectanceMin']"):
                ref_min_XS1 = float(t.text)
            for t in tree.findall(".//Band_Index_List[@band_id='XS2']/QUALITY_INDEX[@name='ReflectanceMax']"):
                ref_max_XS2 = float(t.text)
            for t in tree.findall(".//Band_Index_List[@band_id='XS2']/QUALITY_INDEX[@name='ReflectanceMin']"):
                ref_min_XS2 = float(t.text)
            for t in tree.findall(".//Band_Index_List[@band_id='XS3']/QUALITY_INDEX[@name='ReflectanceMax']"):
                ref_max_XS3 = float(t.text)
            for t in tree.findall(".//Band_Index_List[@band_id='XS3']/QUALITY_INDEX[@name='ReflectanceMin']"):
                ref_min_XS3 = float(t.text)
            print(ref_max_XS1,ref_min_XS1,ref_max_XS2,ref_min_XS2,ref_max_XS3,ref_min_XS3)
            if ref_max_XS1 == 0 or ref_max_XS2 == 0 or ref_max_XS3 == 0 : 
                print("no max ref !")
                continue
            print(date_scene)
            for spot_name in spot_names:
                if spot_name not in ziped_file : continue
                for ins_name in ins_names:
                    if ins_name not in ziped_file : continue
                    if year not in dict_ref[spot_name][ins_name]['years']:
                        dict_ref[spot_name][ins_name]['years'][year] = {}
                        dict_ref[spot_name][ins_name]['years'][year]['XS1'] = {}
                        dict_ref[spot_name][ins_name]['years'][year]['XS2'] = {}
                        dict_ref[spot_name][ins_name]['years'][year]['XS3'] = {}
                        dict_ref[spot_name][ins_name]['years'][year]['XS1']['max'] = []
                        dict_ref[spot_name][ins_name]['years'][year]['XS2']['max'] = []
                        dict_ref[spot_name][ins_name]['years'][year]['XS3']['max'] = []
                        dict_ref[spot_name][ins_name]['years'][year]['XS1']['min'] = []
                        dict_ref[spot_name][ins_name]['years'][year]['XS2']['min'] = []
                        dict_ref[spot_name][ins_name]['years'][year]['XS3']['min'] = []
                        
                    if month not in dict_ref[spot_name][ins_name]['months']:
                        dict_ref[spot_name][ins_name]['months'][month] = {}
                        dict_ref[spot_name][ins_name]['months'][month]['XS1'] = {}
                        dict_ref[spot_name][ins_name]['months'][month]['XS2'] = {}
                        dict_ref[spot_name][ins_name]['months'][month]['XS3'] = {}
                        dict_ref[spot_name][ins_name]['months'][month]['XS1']['max'] = []
                        dict_ref[spot_name][ins_name]['months'][month]['XS2']['max'] = []
                        dict_ref[spot_name][ins_name]['months'][month]['XS3']['max'] = []
                        dict_ref[spot_name][ins_name]['months'][month]['XS1']['min'] = []
                        dict_ref[spot_name][ins_name]['months'][month]['XS2']['min'] = []
                        dict_ref[spot_name][ins_name]['months'][month]['XS3']['min'] = []
                    
                    dict_ref[spot_name][ins_name]['years'][year]['XS1']['max'].append(ref_max_XS1)
                    dict_ref[spot_name][ins_name]['years'][year]['XS2']['max'].append(ref_max_XS2)
                    dict_ref[spot_name][ins_name]['years'][year]['XS3']['max'].append(ref_max_XS3)
                    dict_ref[spot_name][ins_name]['months'][month]['XS1']['max'].append(ref_max_XS1)
                    dict_ref[spot_name][ins_name]['months'][month]['XS2']['max'].append(ref_max_XS2)
                    dict_ref[spot_name][ins_name]['months'][month]['XS3']['max'].append(ref_max_XS3)
                    dict_ref[spot_name][ins_name]['years'][year]['XS1']['min'].append(ref_min_XS1)
                    dict_ref[spot_name][ins_name]['years'][year]['XS2']['min'].append(ref_min_XS2)
                    dict_ref[spot_name][ins_name]['years'][year]['XS3']['min'].append(ref_min_XS3)
                    dict_ref[spot_name][ins_name]['months'][month]['XS1']['min'].append(ref_min_XS1)
                    dict_ref[spot_name][ins_name]['months'][month]['XS2']['min'].append(ref_min_XS2)
                    dict_ref[spot_name][ins_name]['months'][month]['XS3']['min'].append(ref_min_XS3)


with open('data_'+tile+'.json', 'w') as fp:
    json.dump(dict_ref, fp)


with open('data_'+tile+'.json', 'r') as fp:
    dict_ref = json.load(fp)

# calculate time series
for band in ['XS1','XS2','XS3']:
    # Plot figure with subplots 
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    fig.suptitle(band+"_"+tile,size = 14)
    ax1.set_title('Avg max ref per year',size = 9)
    ax2.set_title('Avg max ref per month',size = 9)
    ax1.set(xlabel='years',ylabel='Avg max ref')
    ax2.set(xlabel='months',ylabel='Avg max ref')
    ax3.set_title('Avg min ref per year',size = 9)
    ax4.set_title('Avg min ref per month',size = 9)
    ax3.set(xlabel='years',ylabel='Avg min ref')
    ax4.set(xlabel='months',ylabel='Avg min ref')
    for ins_name in ins_names:
        for spot_name in ['SPOT1','SPOT2']:
            list_years = []
            list_months = []
            list_avg_ref_max_years = []
            list_avg_ref_max_months = []
            list_avg_ref_min_years = []
            list_avg_ref_min_months = []
            for year in sorted(list(map(int,dict_ref[spot_name][ins_name]['years']))):
                list_ref_max = dict_ref[spot_name][ins_name]['years'][str(year)][band]['max']
                list_avg_ref_max_years.append(float(sum(list_ref_max) / len(list_ref_max)))
                list_ref_min = dict_ref[spot_name][ins_name]['years'][str(year)][band]['min']
                list_avg_ref_min_years.append(float(sum(list_ref_min) / len(list_ref_min)))
                list_years.append(year)
            for month in sorted(list(map(int,dict_ref[spot_name][ins_name]['months']))):
                list_ref_max = dict_ref[spot_name][ins_name]['months'][str(month)][band]['max']
                list_avg_ref_max_months.append(float(sum(list_ref_max) / len(list_ref_max)))
                list_ref_min = dict_ref[spot_name][ins_name]['months'][str(month)][band]['min']
                list_avg_ref_min_months.append(float(sum(list_ref_min) / len(list_ref_min)))
                list_months.append(month)
                
           
            ax1.plot(list_years,list_avg_ref_max_years,label = ins_name+"_"+spot_name)
            ax2.plot(list_months,list_avg_ref_max_months,label = ins_name+"_"+spot_name)
            ax3.plot(list_years,list_avg_ref_min_years,label = ins_name+"_"+spot_name)
            ax4.plot(list_months,list_avg_ref_min_months,label = ins_name+"_"+spot_name)

    ax1.legend(ncol=4,framealpha = 0.2,prop={'size': 9})
    ax2.legend(ncol=4,framealpha = 0.2,prop={'size': 9})
    ax3.legend(ncol=4,framealpha = 0.2,prop={'size': 9})
    ax4.legend(ncol=4,framealpha = 0.2,prop={'size': 9})
    #fig.tight_layout()
    fig.set_size_inches(15.5, 8.5)
    fig.savefig(band+"_"+tile+".png")
    plt.close(fig)
