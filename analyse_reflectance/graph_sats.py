#!/usr/bin/python
# -*- coding: utf-8 -*-



import os
import os.path as op
import json
import shutil
import argparse
import tempfile
import glob
from matplotlib.ticker import PercentFormatter
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches
from itertools import cycle
from matplotlib.ticker import MultipleLocator

dictionary_file =  open("satellite_library.json")
dictionary = json.load(dictionary_file)

fig = plt.figure()
list_ins = []
cmap = plt.get_cmap("tab10")
patches = []
band_list = []
for sat in dictionary:
    print(sat)
    for ins in dictionary[sat]:
        print(ins)
        if ins in list_ins : continue
        list_ins.append(ins)
        
        myIterator = cycle([0.2,0,-0.2])
        for band in dictionary[sat][ins]["BANDS"]:
            i = next(myIterator)
            nb_bands = len(dictionary[sat][ins]["BANDS"])
            name = dictionary[sat][ins]["BANDS"][band]["NAME"].split()[0]
            color = ""
            
            if name == "pan" : continue
            if name == "blue" or name == "green" or name == "red" : color = name
            if "nir" in name : color = "firebrick"
            if "swir" in name : color = "darkorange"
            if "vegetation" in name : color = "peru"
            if "water" in name : color = "aqua"
            if "cirrus" in name : color = "grey"
            if "aerosol" in name : color = "magenta"
            if name not in band_list : 
                band_list.append(name)
                patch = mpatches.Patch(color=color, label=name)
                patches.append(patch)
            print(ins,name)
            center = float(dictionary[sat][ins]["BANDS"][band]["BAND CENTER"])
            width = float(dictionary[sat][ins]["BANDS"][band]["BAND WIDTH"])
            min_v= center - width/2
            max_v = center + width/2
            ins_num =  list_ins.index(ins) + i
            plt.plot([min_v,max_v],[ins_num,ins_num], color=color,linewidth=3.0)
            if i >= 0:
                t = 0.1
            else: 
                t = -0.3
            plt.text(center,ins_num + t,band,fontsize='x-small')

      
plt.yticks(range(len(list_ins)), list_ins)
plt.legend(handles=patches,prop={'size': 10},loc='center left', bbox_to_anchor=(0.95, 0.5),fancybox=True, shadow=True)

plt.locator_params(axis="x", nbins=20)
plt.grid()
fig.set_size_inches(15.5, 8.5)
fig.savefig("graph_sat.png",)
plt.close(fig)
